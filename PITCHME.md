#HSLIDE

![LOGO](CBPHeaderLogo2.png)

#### Comic Book Number Crunching Process

<br>
<span style="color:gray">Description of the number crunching process and the various data files available about comic book solicitations and sales</span>

#HSLIDE

(overview diagram goes here)

#HSLIDE

## Top Comics Breakdown

* Publisher
* New Items / Reorder Items / Total Items
* Min Price / Max Price / Average Price
* Total Units
* Total Dollars
* Percent Of Top Comics By Units
* Percent Of Top Comics By Dollars

#HSLIDE

## Top Comics Summary

* Quantity Rank
* Retail Rank
* Diamond Order Index
* Item Code
* M?
* Price
* Publisher
* Title
* Issue

#VSLIDE

### Top Comics Summary (Continued)

* Reorder
* Number Of Previous Times On List
* Estimated Sales
* Previous Issue
* Previous Price
* Price Change
* Previous Issue Estimated Sales
* Delta
* Delta Percentage

#VSLIDE

### Top Comics Summary (Continued)

* Expected Ship Date
* First Ship Date
* Weeks Late
* Original Expected Ship Date
* Total Weeks Late
* Weeks Since Previous Issue
* Solicitation Blurb

#VSLIDE

### Top Comics Summary (Continued)

* Item Code Year
* Item Code Month
* Ven
* Description
* ?

#VSLIDE

### Top Comics Summary (Continued)

* Solicitaition History
* Sales History
* Count

* Returnable?
* Total Units (Returnable items are reported at 80% of invoice)
* Unreported Units (Returnable items are reported at 80% of invoice)

#HSLIDE

### Top Trades Summary

* Quantity Rank
* Retail Rank
* Diamond Order Index
* Item Code
* M
* Price
* Publisher
* Title
* Format

#VSLIDE

### Top Trades Summary (Continued)

* Reorder
* Number Of Previous Times On List
* Estimated Sales
* Total Reported Estimated Sales
* Previous Volume Number
* Previous Volume Total Reported Estimated Sales
* Delta
* Delta Percentage

#VSLIDE

### Top Trades Summary (Continued)

* Expected Ship Date
* First Ship Date
* Weeks Late
* Solicitation Blurb
* Item Code Year
* Item Code Month
* Vendor
* Vol
* ?

#VSLIDE

### Top Trades Summary (Continued)
* Solicitation History
* Collected Edition History
* Number Of Collected Editions

* Total Dollars
* Dollar Rank

#HSLIDE

### Monthly Diamond Sales Data
* Overall Stats / Market Shares
* Top Comics
* Top Trades / Manga
* Top Books
* Top Magazines

#VSLIDE

### Overall Stats
* DCD-Top Sellers MONTH YYYY.doc
* market-share.xls 
* percentages.xls

#VSLIDE

### Top Comics

* top300-comics.xls
* top50-indie.xls
* top50-small.xls

#VSLIDE

### Top Trades

* top300-trades.xls
* top50-indiegn.xls
* top50-smallgn.xls
* top50-manga.xls

#VSLIDE

### Top Books/Magazines

* top10-books.xls
* top10-magazines.xls

#VSLIDE

### Unindexed Data

* top10-apparel.xls
* top10-misc.xls
* top10-posters.xls
* top10-tradingcards.xls
* top10-videos.xls
* top25-games.xls
* top25-toys.xls

#HSLIDE

### Next Data set

